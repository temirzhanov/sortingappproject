package org.example;

import java.util.Arrays;

/** accepts array of integers generated in Main class and sorts the array in ascending
 * order, throwing exceptions for arrays with 0, 1, and more than 10 elements.*/
public class SortingApp {
    /**Sorts the given array in ascending order. Throws IllegalArgumentException in 3 cases: empty input,
     * 1 argument passed, more than 10 arguments passed.
     * @param array array of ints passed in Main class to be sorted
     * @return sorted array
     */
    public static String sort(int[] array) {

        if (array.length == 0) throw new IllegalArgumentException("No arguments");
        if (array.length == 1) throw new IllegalArgumentException("Only 1 argument entered");
        //if (array.length == 10) throw new IllegalArgumentException("Only 1 argument entered");
        if (array.length > 10) throw new IllegalArgumentException("More than 10 arguments entered");
        Arrays.sort(array);
        return Arrays.toString(array);
    }
    /**checks if input contains integers, throwing exceptions if input contains
     * anything else that integers.
     * @param values array of string values to be checked for presence of anything other than integers.*/
    public static void checkString(String[] values) {

        for (String value : values) {
            if (value.isEmpty()) {
                throw new IllegalArgumentException("No arguments");
            }
            if (!value.matches("\\d+")) {
                throw new IllegalArgumentException("All arguments must be integers.");
            }
        }
    }
}
