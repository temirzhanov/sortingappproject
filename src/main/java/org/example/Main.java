package org.example;

import java.util.Scanner;

/** is used to scan a string of up to 10 integers from user input, it splits the input
 * using the separator space and converts the string values to integers. After that sort method is called.
 */
public class Main {
    /**Accepts a string with maximum of 10 integers from user input, splits the string values and converts them into integers.
     * @param args user input of 10 integers*/
    public static void main(String[] args) {

        System.out.println("Please enter up to 10 integers separating them with space.");
        Scanner in = new Scanner(System.in);
        String input = in.nextLine();

        String[] values = input.split(" ");

        SortingApp.checkString(values);

        int[] arguments = new int[values.length];

        for (int i = 0; i < values.length; i++) arguments[i] = Integer.parseInt(values[i]);
        String output = SortingApp.sort(arguments);
        System.out.println(output);
    }
}
